This is an encryption/decryption program using the Vigenere cipher.
usage: input-file output-file cipher-key mode

cipher-key can by any text, but will be converted to solely UPPERCASE letters before use. Default is COLORADOSCHOOLOFMINES.

mode is any of encrypt, decrypt, E, D, e, d.  Default is encrypt.