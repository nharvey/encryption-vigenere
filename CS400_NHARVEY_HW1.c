/*
CSCI 400 HW 1
Created by Nathan Harvey
*/
#include <stdio.h>
#include <stdlib.h>

//Takes two null-terminated strings and returns true (1) if they are equal.
bool streq(char* s1, char* s2);

char * LoadKey(char* rawKey);

//usage: input-file output-file cipher-key mode
int main (int argc, char* argv[]){//argument count and argument values
	printf("Command: \n");
	for(int i = 0; i<argc; i++)printf("%s ", argv[i]);
	printf("\nThere are %i arguments we will use and they are:\n", (5<argc)?4:argc-1);
	for (int i = 1; i < argc; ++i)
	{
		printf("%s\n", argv[i]);
		if(4==i)break;
	}

	FILE* infile = NULL;
	FILE* outfile = NULL;
	bool decrypt = 0;//encrypt is default
	char* key;
	int k = 0;//which key letter we are using
	char* labels[8] = {"Lines read","Characters","Letters","lowercase","UPPERcase","Numbers","Whitespace","Special Chars"};
	int stats[8] = {};
	stats[0]=1;//assume at least one line
	char cur=0;//current character.
	short group = 0;//which character of the 5-group we are on.

	if(2>argc){printf("You need to supply the file to read data from.\n");exit(EXIT_FAILURE);}
	infile = fopen(argv[1],"rt");
  		if (!infile) // Input file failed to open.
  		{
  			printf("BAD NEWS: It is impossible to read that input file (%s).\n", argv[1]);
  			exit(EXIT_FAILURE);
  		}
  	if(3<=argc){
  		outfile = fopen(argv[2],"wt");
  		if (!outfile) // Output file failed to open.
  		{
  			printf("SORTA BAD NEWS: That output file isn't writeable. (%s).\n", argv[2]);
  		}
  		if(streq(argv[1],argv[2])){//check to see if input and output are the same
  			printf("Let's not overwrite the file we are reading from, ok?\n");
  			exit(EXIT_FAILURE);
  		}
  	}
  	if(!outfile)printf("writing to screen...\n"); 
  	//either failed to open writeable or no outfile specified.

	if(4<=argc) key = LoadKey(argv[3]);
	else key = LoadKey("Colorado School of Mines");
	if(NULL==key){
		printf("Out of memory.\n");
		exit(EXIT_FAILURE);
	}
	if(5<=argc&&('d'==argv[4][0]||'D'==argv[4][0]))decrypt = 1;
	
	while(true){
		cur=fgetc(infile);
		if(EOF==cur)break;
		if(0==key[k])k=0;
		stats[1]++;//everything counts as a character.
		if(('A'<=cur&&'Z'>=cur)||('a'<=cur&&'z'>=cur)) {//If it is a letter, go ahead and encrypt/decrypt
			stats[2]++;//letter
			if('a'<=cur){
				cur-='a';//cur is now an int from 0 to 25
				stats[3]++;//lowercase
			}else{
				cur-='A';//cur is now an int from 0 to 25
				stats[4]++;//UPPERcase
			}
			cur=((key[k]-'A')*(1-decrypt*2)+cur)%26 + 'A';//CHANGE THIS TO SOMETHING OTHER THAN ZERO!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			if('A'>cur)cur+=26;
			if(5==group++){
				fputc(' ',outfile);
				group=1;
			}
			fputc(cur,outfile);
		}else if('0'<=cur&&'9'>=cur) stats[5]++;
		else if(' '==cur||'\t'==cur||'\n'==cur||'\v'==cur) stats[6]++;
		else stats[7]++;
		if('\n'==cur)stats[0]++;//count a new line
	}

	for(int i = 0; i<8; i++){
		printf("%s:\t%i\n",labels[i], stats[i]);
	}

	delete[] key;
	fclose(infile);
	if(outfile){
		fputc('\n',outfile);//makes it easier for those of us who read with cat.
		fclose(outfile);
	}

	return EXIT_SUCCESS;
}

bool streq(char* s1, char* s2){
	for(int i = 0; ;i++){
		if(s1[i]!=s2[i])return 0;
		if(0==s1[i]||0==s2[i]){
			if(s1[i]==s2[i])break;//stop when the strings have ended.
			else return 0;//if they don't both end now, they are different lengths
		}
	}
	return 1;//should only get here via break statement
}


char * LoadKey(char* rawKey){
	int size = 4;
	char* key = new char[size];
	if(!key)return NULL;
	int k = 0;//the letter we are editing on the sanitized key
	int i;//the letter we are looking at on the original key
	for(i=0;;++i){
		if(size==i+1){//expand the dynamic array if needed.
			size*=2;
			char* big = new char[size];
			if(!big)return NULL;//too big or out of memory
			for (int i = 0; i < size/2; ++i) big[i] = key[i];
			delete[] key;
			key = big;
		}
		if('A'<=rawKey[i]&&'Z'>=rawKey[i]) key[k]=rawKey[i];
		else if('a'<=rawKey[i]&&'z'>=rawKey[i]) key[k]=rawKey[i]+'A'-'a';
		else if(0==rawKey[i]){//reached end of string
			key[k] = rawKey[i];//terminate the other string
			break;
		}
		else continue;
		k++;
	}
	//printf("%s\n", key);
	return key;
}

//These lines are in case I need to invoke undefined behaviour.
//		printf("Would you like to play a game, Alex?\n");
//		int u = u++ + ++u;//start a thermonuclear war